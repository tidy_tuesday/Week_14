
# Tidy Tuesday - Week 14
### Global Life Expectancy

My contribution to week 14 of TidyTuesday. I had a few ideas for this one, but only one worked out.

See https://github.com/rfordatascience/tidytuesday for more information.

#### CAGR - 1950 to 2015
![](life_expectancy_cagr.png)

#### Total Change - 1950 to 2015
![](life_expectancy_chng.png)

#### 1950 Median
![](life_expectancy_1950.png)

#### 2015 Median
![](life_expectancy_2015.png)

